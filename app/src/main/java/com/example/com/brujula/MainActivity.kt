package com.example.com.brujula

import android.content.Context.SENSOR_SERVICE
import android.hardware.Sensor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.hardware.SensorManager
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.Sensor.TYPE_GYROSCOPE
import android.widget.ImageView

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.widget.ListView;
import android.support.v4.view.ViewCompat.setRotation
import android.support.v4.view.ViewCompat.setRotation
import android.widget.TextView


class MainActivity : AppCompatActivity(), SensorEventListener {
    override fun onSensorChanged(event: SensorEvent?) {
        if (event != null) {
            when (event.sensor.type) {
                Sensor.TYPE_ACCELEROMETER -> mGravity = event.values
                Sensor.TYPE_MAGNETIC_FIELD -> mGeomagnetic = event.values
            }
        }
        if (mGravity != null && mGeomagnetic != null) {
            val RotationMatrix = FloatArray(16)
            val success = SensorManager.getRotationMatrix(RotationMatrix, null, mGravity, mGeomagnetic)
            if (success) {
                val orientation = FloatArray(3)
                SensorManager.getOrientation(RotationMatrix, orientation)
                azimut = orientation[0] * (180 / Math.PI.toFloat())
            }
        }
        val grados = azimut*-1
        iv!!.setRotation(grados)
        tv!!.setText("Grados: " + grados)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    private var sensorManager: SensorManager? = null
    var giroscopio: Sensor? = null
    var acelerometro: Sensor? = null
    var mGravity: FloatArray? = null
    var mGeomagnetic: FloatArray? = null
    var azimut: Float = 0.toFloat()
    var iv: ImageView? = null
    var tv: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager?;
        acelerometro = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        giroscopio = sensorManager!!.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        iv = findViewById(R.id.imageView);
        tv = findViewById(R.id.textView);
        if (giroscopio != null)
        {
            sensorManager!!.registerListener(this, giroscopio, SensorManager.SENSOR_DELAY_NORMAL);
            sensorManager!!.registerListener(this, acelerometro, SensorManager.SENSOR_DELAY_NORMAL);
        }


    }
}
